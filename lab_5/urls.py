from django.conf.urls import url
from .views import index, add_todo,delete_button

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^delete_button', delete_button, name='delete_button'),
]
