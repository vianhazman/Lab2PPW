storage = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]
var backgroundColor;
var textColor;
var index;
//fungsi ganti tema
function changeTheme(bg, color){
  $("body").css({"backgroundColor": bg});
  $("h1").css({"color": color});
}
//Implementasi pergantian tema
if (localStorage.getItem("index") === null){
  index = 3;
}else{
  index = JSON.parse(localStorage.getItem("index"));
}
backgroundColor = storage[index]["bcgColor"];
textColor = storage[index]["fontColor"];
console.log(storage[index]["bcgColor"]);
changeTheme(backgroundColor, textColor);

$(document).ready(function(){
  $("#my-select").select2({
    "data" : storage
  });
  $("#my-select").val(index).change();
  $("#themebtn").on("click", function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select
      index = $("#my-select").val();
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
      backgroundColor = storage[index]["bcgColor"];
      textColor = storage[index]["fontColor"];
      // [TODO] ambil object theme yang dipilih
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      changeTheme(backgroundColor, textColor);
      // [TODO] simpan object theme tadi ke local storage selectedTheme
      localStorage.setItem("index", JSON.stringify(index));
  })
});

$(document).ready(function(){
	console.log("hey");
	refresh();
    $("#toggle").click(function(){
        $(".chat-body").toggle(500);
    });
});




var print = document.getElementById('print');
	var erase = false;

	var go = function(x) {
  if (x === 'ac') {
    print.value = '';
    erase = false;
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value)*10000)/10000;
    erase = true;
  } else if (x === 'log' || x ==='sin' || x === 'tan') {
    switch (x) {
      case 'log':
        print.value = Math.log10(print.value);
        break;
      case 'sin':
        print.value = Math.sin(print.value);
        break;
      case 'tan':
        print.value = Math.sin(print.value);
        break;
    }
  } else {
    print.value += x;
  }
}

function evil(fn) {
  return new Function('return ' + fn)();
}
	// END

// chat box

  var displayChat = document.getElementById('msg');

     if(!localStorage.getItem("chat")){
     localStorage.setItem("chat", JSON.stringify([]));
     }
     var chatHistory = JSON.parse(localStorage.getItem("chat"));


     var send = document.getElementById('enter');
     var i =0;
     send.addEventListener('keypress', function (e) {
     	var key = e.which || e.keyCode;
     	if (key===13) {
     		if (document.getElementById('enter').value === ""){
     		alert("Please insert message")
             return
         }

         else if (document.getElementById('enter').value === "/clear"){
         	displayChat.innerHTML ='';
         	document.getElementById('enter').value = "";
         	localStorage.clear();
             return
         }

         var message={
             text : document.getElementById('enter').value,
             dateTime : new Date().toLocaleTimeString() + " " + new Date().toDateString(),
         };
         chatHistory.push(message);
         document.getElementById('enter').value = "";
         localStorage.setItem("chat", JSON.stringify(chatHistory));

         localData = localStorage.getItem("chat");
         localData = JSON.parse(localData);
     	}
         
     });

const chatBot = function(message){
  const corsProxy = 'https://cors-anywhere.herokuapp.com/';
  const botURL = 'http://www.cleverbot.com/getreply?key=CC5agXq7qqWbZxYhjlgWn8m9QZg&input='+message+"&cs="+chatCS;
  axios.get(corsProxy+botURL).then(function (response){
    console.log(response.data.output);
    chatCS = response.data.cs;
    displayChat.innerHTML+= "<p class='msg-receive'>"+response.data.output+"</p>";
  });
}

     function refresh() {
     localData = localStorage.getItem("chat");
     localData = JSON.parse(localData);
     for (var i = 0 ; i < localData.length; i++) {
     	var templateDiv =
             "<div class='message'>" +
             "<p class='text'>" + localData[i].text + "</p>" + "<div class='clear'></div>" + "</div>"+
         "<p class='datetime'>" + localData[i].dateTime + "</p>" + "<div class='clear'></div>";
         displayChat.innerHTML += templateDiv;
     }
 }

 